import React, { Fragment } from 'react'

const Laps = ({lap,no}) => {
    return (
        <Fragment>
            <div className="laps">
                <div className="time">
                {/* <p>Lap {no}</p> */}
                    <p> Lap {no}{"- "}
                        {lap.h} :{" "}
                        {lap.m} :{" "}
                        {lap.s} :{" "}
                        {lap.ms}
                    </p>
                </div>
            </div>
        </Fragment>
    )
}

export default Laps