import React, { Fragment } from 'react'

const StartButton = (props) => {
    const start=props.start;
    const setNum=props.setNum;
    const merged=()=>{
          start(true);
          setNum(1);
    }
  return (
    <Fragment>

       <button disabled={props.state} className={props.state?"disabled":""} onClick={merged}>{props.num===0?"Start":"Resume"}</button>
    </Fragment>
  )
}

export default StartButton