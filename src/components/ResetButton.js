import React from 'react'

const ResetButton = (props) => {
    const reset=props.reset;
  return (
    <>
    {props.num!==0 && !props.state&& <button disabled={props.state} className={props.state?"disabled":""} onClick={()=>reset(0)}>Reset</button>}
    </>
  )
}

export default ResetButton