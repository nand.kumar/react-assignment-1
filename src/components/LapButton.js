import React, { Fragment } from 'react'

const LapButton = ({lap,num,state}) => {
  return (
    <Fragment>

      {num!==0 &&<button onClick={lap} disabled={!state}  className={!state?"disabled":""}>Lap</button>}
    </Fragment>
  )
}

export default LapButton