import React, { Fragment } from 'react'

const Timer = (props) => {
  const time = props.time;
  return (
    <Fragment>
      <div className="timer">
        <span>{("0" + Math.floor((time / 3600000) % 60)).slice(-2)}</span>:
        <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}</span>:
        <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}</span>:
        <span className='ms'>{("0" + ((time / 10) % 100)).slice(-2)}</span>
      </div>
    </Fragment>
  )
}

export default Timer