import React, { Fragment } from 'react'

const ClearLaps = ({clearLap}) => {
  return (
      <Fragment>
          <button onClick={clearLap}>Clear Lap</button>
      </Fragment>
  )
}

export default ClearLaps