import React from 'react'

const StopButton = (props) => {
    const stop=props.stop;
  return (
    <>
   {props.num!==0 && <button disabled={!props.state} className={!props.state?"disabled":""} onClick={()=>stop(false)}>Stop</button>}
    </>
  )
}

export default StopButton